# Create node image
FROM node:lts-alpine

# specifi the workdir
WORKDIR /app

# copy the files
COPY . /app

EXPOSE 8000

CMD [ "npm", "run", "start" ]
